/**
 * @author Harro Lissenberg
 */
public class Nutrient {
    String code;
    String name;
    String unit;
    String name_en;

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Nutrient");
        sb.append("{code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", unit='").append(unit).append('\'');
        sb.append(", name_en='").append(name_en).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
