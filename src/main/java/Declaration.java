/**
 * @author Harro Lissenberg
 */
public class Declaration {
    String  nutrientCode;
    String amount;

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Declaration");
        sb.append("{nutrientCode='").append(nutrientCode).append('\'');
        sb.append(", amount='").append(amount).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
