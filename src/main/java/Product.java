import java.util.ArrayList;
import java.util.List;

/**
 * @author Harro Lissenberg
 */
public class Product {

    Group group = new Group();
    String code;
    String description;

    List<Declaration> declarations;

    public void add(Declaration declaration) {
        if(declarations == null) {
            declarations = new ArrayList<>();
        }
        declarations.add(declaration);
    }

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Product");
        sb.append("{group=").append(group);
        sb.append(", code='").append(code).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", declarations=").append(declarations);
        sb.append('}');
        return sb.toString();
    }
}
