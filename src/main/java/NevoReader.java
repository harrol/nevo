import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Harro Lissenberg
 */
public class NevoReader {

    private static final Logger LOG = Logger.getLogger(NevoReader.class.getName());

    private Map<String, Nutrient> nutrientMap;
    List<Product> products;

    public void loadNutrients(String nutrientsFile) {
        nutrientMap = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(nutrientsFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                Nutrient nutrient = parseNutrient(line);
                if (nutrient != null) {
                    nutrientMap.put(nutrient.code, nutrient);
                }
            }
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Could not read nutrients file: " + nutrientsFile, e);
        }
    }

    public void loadProducts(String productsFile) {
        products = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(productsFile))) {
            while (true) {
                Product p = parseProduct(br);
                parseNutrients(br, p);
                products.add(p);
                if (br.readLine() == null) {
                    break;
                }
            }

        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Could not read products file: " + productsFile, e);
        }
    }

    //    ----------
//    Productgroep-oms        Aardappelen
//    Productgroepcode        1
//    Productcode             1
//    Controlegetal           4
//    Product_omschrijving    Aardappelen rauw
//    Fabrikantnaam
//    Code_nonactief          N
//    Hoeveelheid             100
//    Meeteenheid             g
//    Eetbaar_gedeelte        1
//    Vertrouwelijk_code      N
//    Commentaarregel         Per 100g
//    Nutrientcode  Gehalte  Broncode     Datum_mutatie  Spoor
//    ------------  -------  -----------  -------------  -----
//    01001              88  13.00000         28FEB2011
//    01002             371  13.00000         28FEB2011
    private Product parseProduct(BufferedReader reader) throws IOException {
        Product p = new Product();
        String line = reader.readLine();
        if (line.equals("----------")) {
            line = reader.readLine();
        }
        p.group.description = line.substring(24).trim();
        p.group.code = reader.readLine().substring(24).trim();
        p.code = reader.readLine().substring(24).trim();
        reader.readLine();
        p.description = reader.readLine().substring(24).trim();
        reader.readLine();
        reader.readLine();
        reader.readLine();
        reader.readLine();
        reader.readLine();
        reader.readLine();
        reader.readLine();
        return p;
    }

    private void parseNutrients(BufferedReader br, Product product) throws IOException {
        br.readLine(); // Nutrients header
        br.readLine(); // Header separator
        String line = br.readLine(); // First nutrient declaration
        while (line != null && !line.trim().equals("----------")) {
            parseDeclaration(product, line);
            line = br.readLine();
        }
    }

    private void parseDeclaration(Product product, String line) {
        if (line != null && line.length() >= 11) {
            Declaration d = new Declaration();
            d.nutrientCode = line.substring(0, 5).trim();
            d.amount = line.substring(11, 21).trim();
            product.add(d);
        }
    }

    private Nutrient parseNutrient(String line) {
        if (line != null && line.trim().length() >= 50) {
            Nutrient nutrient = new Nutrient();
            nutrient.code = line.substring(0, 5).trim();
            nutrient.name = line.substring(7, 37).trim();
            nutrient.unit = line.substring(39, 44).trim();
            nutrient.name_en = line.substring(49).trim();
            return nutrient;
        }
        return null;
    }

    public Map<String, Nutrient> getNutrientMap() {
        return nutrientMap;
    }

    public List<Product> getProducts() {
        return products;
    }

}
