/**
 * @author Harro Lissenberg
 */
public class Main {

    public static final String nutrients = "/Users/harro/projects/nevo/data/Nevo-Nutrienten_Lijst_Online versie 2011_3.0.txt";
    public static final String nevo_dat = "/Users/harro/projects/nevo/data/Nevo-Online versie 2011_3.0.dat";

    public static void main(String[] args) {
        NevoReader nevoReader = new NevoReader();
        nevoReader.loadNutrients(nutrients);
        System.out.println("Nutrients in database: " + nevoReader.getNutrientMap().size());
        nevoReader.loadProducts(nevo_dat);
        System.out.println("Products in the database: " + nevoReader.getProducts().size());
    }
}
