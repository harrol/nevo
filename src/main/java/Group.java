/**
 * @author Harro Lissenberg
 */
public class Group {

    String description;
    String code;

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Group");
        sb.append("{description='").append(description).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
